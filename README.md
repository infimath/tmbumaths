[![Netlify Status](https://api.netlify.com/api/v1/badges/6a47e7d2-ee16-48fe-9d11-8e766bc86dbf/deploy-status)](https://app.netlify.com/sites/agitated-albattani-7470cc/deploys)

# bootstrap

> My super-excellent Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
